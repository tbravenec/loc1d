import torch
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.lin_1 = nn.Linear(16, 512)
        self.lin_2 = nn.Linear(self.lin_1.out_features, 256)
        self.lin_3 = nn.Linear(self.lin_2.out_features, 512)
        self.lin_4 = nn.Linear(self.lin_3.out_features, 256)
        self.lin_5 = nn.Linear(self.lin_4.out_features, 128)
        self.lin_6 = nn.Linear(self.lin_5.out_features, 64)
        self.lin_7 = nn.Linear(self.lin_6.out_features, 32)
        self.lin_8 = nn.Linear(self.lin_7.out_features, 1)

    def forward(self, x):
        x = F.relu(self.lin_1(x))
        x = F.relu(self.lin_2(x))
        x = F.relu(self.lin_3(x))
        x = F.relu(self.lin_4(x))
        x = F.relu(self.lin_5(x))
        x = F.relu(self.lin_6(x))
        x = F.relu(self.lin_7(x))
        x = F.relu(self.lin_8(x))
        return x