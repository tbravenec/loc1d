import numpy as np
import pandas as pd
import os

import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

# from Net_Initial import Net
from Net_Initial_plus_channel import Net
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

def main():
    dists = np.arange(0, 35.5, 0.5)
    dfs = []

    # Data loading
    for dist in dists:
        if dist.is_integer():
            fname = 'data/dist.' + '{0:.0f}'.format(dist) + 'm.txt'
        else:
            fname = 'data/dist.' + '{0:.1f}'.format(dist) + 'm.txt'
    
        df = pd.read_csv(fname, header = None, names=['time','tag id','anchor id','anchor1 id', 'anchor2 id', 'anchor3 id','a0ch0','a0ch1','a0ch2','a0ch3','a0rssi0','a0rssi1','a0rssi2','a0rssi3','a1ch0','a1ch1','a1ch2','a1ch3','a1rssi0','a1rssi1','a1rssi2','a1rssi3','a2ch0','a2ch1','a2ch2','a2ch3','a2rssi0','a2rssi1','a2rssi2','a2rssi3','a3ch0','a3ch1','a3ch2','a3ch3','a3rssi0','a3rssi1','a3rssi2','a3rssi3'])
        df['dist'] = dist
        dfs.append(df)

    df = pd.concat(dfs)
    df = df.drop(axis=1,labels = ['time'])
    df = df.drop(axis=1,labels = ['tag id','anchor id','anchor1 id','anchor2 id','anchor3 id'])

    # Data split
    X_train, X_test, y_train, y_test = train_test_split(df.drop(axis=1,labels = ['dist']), df['dist'], test_size=0.3, random_state=0)

    # Test and Train dataset preparations
    torch_X_train = torch.tensor(X_train.values, dtype=torch.float)
    torch_X_test = torch.tensor(X_test.values, dtype=torch.float)
    torch_Y_train = torch.tensor(y_train.values, dtype=torch.float)
    torch_Y_test = torch.tensor(y_test.values, dtype=torch.float)
    
    # Use CUDA if wanted and available in system (Don't, network is too small for benefits of GPU)
    use_cuda = False and torch.cuda.is_available()

    if use_cuda:
        torch_X_train = torch_X_train.cuda()
        torch_X_test = torch_X_test.cuda()
        torch_Y_train = torch_Y_train.cuda()
        torch_Y_test = torch_Y_test.cuda()

    train_dataset = TensorDataset(torch_X_train, torch_Y_train)
    test_dataset = TensorDataset(torch_X_test, torch_Y_test)

    # Model training parameters
    model_path = "model.pth"
    batch_size = 1 # 1 is extremely slow, but results are most accurate, 20 is faster, but not as accurate
    learning_rate = 0.00001
    epochs = 100
    log_interval = 100

    # Create instance of the network
    model = Net()

    # Create a Stochastic Gradient Descent optimizer
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9)
    # Create a Mean Squared Error loss function
    criterion = nn.MSELoss()
    

    # Move model to GPU
    if use_cuda:
        model.cuda()

    train_loader = DataLoader(train_dataset, batch_size)
    test_loader = DataLoader(test_dataset)

    if not os.path.exists(model_path):
        # Set model to training mode
        model.train()
        # Run the main training loop
        for epoch in range(epochs):
            for batch_idx, (data, target) in enumerate(train_loader):
                # Resize data according to batch_size and last input batch_size
                target = target.view(data.shape[0], -1)

                if use_cuda:
                    data = data.cuda()
                    target = target.cuda()

                # Zero the parameter gradients
                optimizer.zero_grad()

                # Forward + Backward + Optimize
                net_out = model(data)

                if use_cuda:
                    net_out.cpu()

                loss = criterion(net_out, target)
                loss.backward()
                optimizer.step()

                # Print statistics
                if batch_idx % log_interval == 0:
                    print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(epoch, batch_idx * len(data), 
                                                                                   len(train_loader.dataset), 
                                                                                   100. * batch_idx / len(train_loader), 
                                                                                   loss.item()))
        # Save the trained model
        torch.save(model.state_dict(), model_path)
    else:
        # Load the trained model
        model.load_state_dict(torch.load(model_path))

    # Set model to evaluation mode
    model.eval()

    x = list()
    y = list()
    list_out = list()
    # Run a test loop
    test_loss = 0
    with torch.no_grad():
        for id, (data, target) in enumerate(test_loader):
            # Resize data according to batch_size and last input batch_size
            target = target.view(data.shape[0], -1)
            net_out = model(data)
            # Sum up batch loss
            test_loss += criterion(net_out, target).item()
            print("Expected/Predicted: {:7.4f}m/{:7.4f}m Difference: {:7.4f}cm".format(target.item(), net_out.item(), abs(target.item() - net_out.item()) * 100))
            list_out.append("Expected/Predicted: {:7.4f}m/{:7.4f}m Difference: {:7.4f}cm\n".format(target.item(), net_out.item(), abs(target.item() - net_out.item()) * 100))
            x.append(target.item())
            y.append(net_out.item())

    test_loss /= len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}\n'.format(test_loss))

    with open("Output.txt", 'w') as file:
        file.writelines(list_out)

    # Plotting the predicted distances vs real distances:
    plt.scatter(x,y,marker='x')
    plt.title("PyTorch")
    plt.xlabel('Real distance [m]')
    plt.ylabel('Predicted distance [m]')
    plt.grid(True)
    plt.show()

    err = pd.Series(np.asarray(y) - np.asarray(x))
    print(err.describe())

    srterr = sorted(abs(err))

    plt.hist(srterr, bins=1000, cumulative=True, density=True)
    plt.show()


if __name__ == "__main__":
    main()
