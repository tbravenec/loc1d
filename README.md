# Loc1D
Analysis of 1-dimensional location data for the FEWERCON project

In order to run/modify the script **data_exploration**.ipynb, please navigate to:
https://colab.research.google.com/github/slaninam/Loc1D/blob/master/data_exploration.ipynb

In order to run Loc1D_pytorch.py, the package pytorch has to be installed according to:
https://pytorch.org/get-started/locally/
As well as packages Pandas, Numpy, Matplotlib and sklearn (Data preparations is the same as in the Jupyter notebook or classic script version of the Jupyter notebook Loc1D.py)