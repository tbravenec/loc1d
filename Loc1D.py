import numpy as np
import pandas as pd

from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

def main():
    dists = np.arange(0, 35.5, 0.5)
    dfs = []

    for dist in dists:
        if dist.is_integer():
            fname = 'data/dist.' + '{0:.0f}'.format(dist) + 'm.txt'
        else:
            fname = 'data/dist.' + '{0:.1f}'.format(dist) + 'm.txt'
    
        df = pd.read_csv(fname, header = None, names=['time','tag id','anchor id','anchor1 id', 'anchor2 id', 'anchor3 id','a0ch0','a0ch1','a0ch2','a0ch3','a0rssi0','a0rssi1','a0rssi2','a0rssi3','a1ch0','a1ch1','a1ch2','a1ch3','a1rssi0','a1rssi1','a1rssi2','a1rssi3','a2ch0','a2ch1','a2ch2','a2ch3','a2rssi0','a2rssi1','a2rssi2','a2rssi3','a3ch0','a3ch1','a3ch2','a3ch3','a3rssi0','a3rssi1','a3rssi2','a3rssi3'])
        df['dist'] = dist
        dfs.append(df)

    df = pd.concat(dfs)
    df = df.drop(axis=1,labels = ['time'])
    df = df.drop(axis=1,labels = ['tag id','anchor id','anchor1 id','anchor2 id','anchor3 id'])

    X_train, X_test, y_train, y_test = train_test_split(df.drop(axis=1,labels = ['dist']), df['dist'], test_size=0.3, random_state=0)

    clf = MLPRegressor(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(40, 30), random_state=0)

    clf.fit(X_train, y_train)
    clf.score(X_test, y_test)

    x = y_test
    y = clf.predict(X_test)

    plt.scatter(x,y,marker='x')
    plt.title("sklearn")
    plt.xlabel('Real distance [m]')
    plt.ylabel('Predicted distance [m]')
    plt.grid(True)
    plt.show()

    err = y-x
    print(err.describe())

    srterr = sorted(abs(err))

    plt.hist(srterr, bins=1000, cumulative=True, density=True)
    plt.show()
    input()


if __name__ == "__main__":
    main()
